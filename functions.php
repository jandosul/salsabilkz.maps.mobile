<?php

function include_function(){
	wp_enqueue_style("bootstrap", get_template_directory_uri() . "/assets/bootstrap/css/bootstrap.min.css");
	wp_enqueue_style("style-name", get_stylesheet_uri());
	wp_enqueue_style("style-dop", get_template_directory_uri() . "/assets/new.css" );
	wp_enqueue_style("fontello", get_template_directory_uri() . "/assets/fontello-1/css/fontello.css" );
	wp_enqueue_style("style-dop", get_template_directory_uri() . "/assets/new.css" );
     
     
    wp_deregister_script( "jquery" );
    wp_register_script("jquery", "//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js");
    wp_enqueue_script("jquery");


	wp_enqueue_script("poper", "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js", array(), false, true);
	wp_enqueue_script("bt-js", get_template_directory_uri() . "/assets/bootstrap/js/bootstrap.min.js", array(), false, true);

}

add_action( 'widgets_init', 'register_my_widgets' );
function register_my_widgets(){
	register_sidebar( array(
		'name'          => "sidebar",
		'id'            => "sidebar-45",
		'description'   => 'eto sidebar'
	) );
}

add_action("wp_enqueue_scripts", "include_function");
add_action("after_setup_theme", "end_setup");
add_image_size( "carousel_slide", $width = 300, $height = 300, $crop = false );
add_image_size( 'sv75', 75, 75 );
add_image_size( 'sv77', 75, 75 );
add_image_size('card-img',120,120);

function end_setup(){
	add_theme_support( "post-thumbnails" );
	register_nav_menus(array("end" => "меню в шапке"));
	
}
// hello

// function single_gile(){
//      wp_enqueue_style( "single-css", get_template_directory_uri() . "/assets/page1.css");
//      wp_enqueue_style("bootstrap", get_template_directory_uri() . "/assets/bootstrap/css/bootstrap.min.css");

// }

// function my_custom_style() {
//    if(is_page_template('single.php')) {
//    wp_enqueue_style( 'style', get_template_directory_uri() . '/assets/page1.css' );
//   }
// }
// add_action( 'wp_enqueue_scripts', 'my_custom_style' );






function hide_admin_bar(){ 
	// if(is_single() && is_singular('namazhana')):
	// 	return false; 
	// endif;
	return false;
}
add_filter( 'show_admin_bar', 'hide_admin_bar' );
	// хук для регистрации




function cptui_register_my_cpts() {

	/**
	 * Post Type: Намазхана.
	 */

	$labels = array(
		"name" => __( "Намазхана", "twentynineteen" ),
		"singular_name" => __( "Намазхана", "twentynineteen" ),
		"all_items" => __( "Намазханалар", "twentynineteen" ),
	);

	$args = array(
		"label" => __( "Намазхана", "twentynineteen" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "namazhana", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail" ),
		"taxonomies" => array( "category" ),
	);

	register_post_type( "namazhana", $args );

	/**
	 * Post Type: Асхана.
	 */

	$labels = array(
		"name" => __( "Асхана", "twentynineteen" ),
		"singular_name" => __( "Асхана", "twentynineteen" ),
		"all_items" => __( "Асханалар", "twentynineteen" ),
	);

	$args = array(
		"label" => __( "Асхана", "twentynineteen" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "asxana", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "asxana", $args );
}

add_action( 'init', 'cptui_register_my_cpts' );



function cptui_register_my_cpts_namazhana() {

	/**
	 * Post Type: Намазхана.
	 */

	$labels = array(
		"name" => __( "Намазхана", "twentynineteen" ),
		"singular_name" => __( "Намазхана", "twentynineteen" ),
		"all_items" => __( "Намазханалар", "twentynineteen" ),
	);

	$args = array(
		"label" => __( "Намазхана", "twentynineteen" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "namazhana", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail" ),
		"taxonomies" => array( "category" ),
	);

	register_post_type( "namazhana", $args );
}

add_action( 'init', 'cptui_register_my_cpts_namazhana' );











function cptui_register_my_cpts_asxana() {

	/**
	 * Post Type: Асхана.
	 */

	$labels = array(
		"name" => __( "Асхана", "twentynineteen" ),
		"singular_name" => __( "Асхана", "twentynineteen" ),
		"all_items" => __( "Асханалар", "twentynineteen" ),
	);

	$args = array(
		"label" => __( "Асхана", "twentynineteen" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "asxana", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "editor", "thumbnail" ),
	);

	register_post_type( "asxana", $args );
}

add_action( 'init', 'cptui_register_my_cpts_asxana' );









function cptui_register_my_taxes() {

	/**
	 * Taxonomy: Категория.
	 */

	$labels = array(
		"name" => __( "Категория", "twentynineteen" ),
		"singular_name" => __( "Категория", "twentynineteen" ),
	);

	$args = array(
		"label" => __( "Категория", "twentynineteen" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => false,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'taxasxana', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "taxasxana",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		);
	register_taxonomy( "taxasxana", array( "asxana" ), $args );
}
add_action( 'init', 'cptui_register_my_taxes' );










function cptui_register_my_taxes_taxasxana() {

	/**
	 * Taxonomy: Категория.
	 */

	$labels = array(
		"name" => __( "Категория", "twentynineteen" ),
		"singular_name" => __( "Категория", "twentynineteen" ),
	);

	$args = array(
		"label" => __( "Категория", "twentynineteen" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => false,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => array( 'slug' => 'taxasxana', 'with_front' => true, ),
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "taxasxana",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		);
	register_taxonomy( "taxasxana", array( "asxana" ), $args );
}
add_action( 'init', 'cptui_register_my_taxes_taxasxana' );



?>
