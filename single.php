<?php get_header( "single" );  ?>
<?php the_title(); ?>


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Фильтр</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
     <div id="mainMenuBody" class="list-group list-group-flush">
	<a href="#cats" class="list-group-item list-group-item-action menu-item" data-menuid="menu_category">Категории товаров<span class="float-right" style="font-weight:bold"><i class="icon-angle-right"></i></span></a>
	<a href="#brands" class="list-group-item list-group-item-action menu-item" data-menuid="brands_ru">Бренды<span class="float-right" style="font-weight:bold"><i class="icon-angle-right"></i></span></a>
	<a href="#health" class="list-group-item list-group-item-action menu-item" data-menuid="health_ru">Здоровье<span class="float-right" style="font-weight:bold"><i class="icon-angle-right"></i></span></a>
	<a href="#bycountries" class="list-group-item list-group-item-action menu-item" data-menuid="countries_ru">По странам<span class="float-right" style="font-weight:bold"><i class="icon-angle-right"></i></span></a>
	<a href="https://www.salsabil.kz/shop/" class="list-group-item list-group-item-action ">Все товары<span class="float-right" style="font-weight:bold"><i class="icon-angle-right"></i></span></a>
	<a href="#about-company" class="list-group-item list-group-item-action menu-item" data-menuid="about_company">О магазине<span class="float-right" style="font-weight:bold"><i class="icon-angle-right"></i></span></a>
	<a href="https://www.salsabil.kz/kontakty/" class="list-group-item list-group-item-action ">Контакты<span class="float-right" style="font-weight:bold"><i class="icon-angle-right"></i></span></a>
</div>      	 
      </div>
    </div>
  </div>
</div>


<body>

	<main>
		<div class="card m-1" style="max-width: 540px;">
		<div class="row no-gutters">	
		  <div class="col-4">
		  	<?php  the_post_thumbnail(""); ?>
		  	</div>
		  	<div class="col-8 d-flex align-items-start flex-column ">
		  		<div class="card-body p-1 small">
		  				<h6 class="m-0"><a href="#" class="stretched-link">Намазхана в столовой Тагам</a></h6>
		  				<section class="border-bottom">Столовая Тагам на Абая Правды</section>
		  				<section class=""><strong>Адрес:</strong> пр. Абая 52</section>
		  		</div>
		  		<div class="card-footer border-0 mt-auto p-1 bg-white small">
		  				Сейчас: <span class="text-danger font-weight-bold">закрыто</span>
		  		</div>
			</div>
		</div>
	</div>



 <div class="card m-1" style="max-width: 540px;">
		<div class="row no-gutters">	
		  <div class="col-4">
		  	<img class="card-img" src="img/ccard">

		  	</div>
		  	<div class="col-8 d-flex align-items-start flex-column ">
		  		<div class="card-body p-1 small">
		  				<h6 class="m-0"><a href="#" class="stretched-link">Мечеть</a></h6>
		  				<section class="border-bottom">Интернет магазин восточных товаров</section>
		  				<section class=""><strong>Адрес:</strong> Аксай 46, дом 15</section>
		  		</div>
		  		<div class="card-footer border-0 mt-auto p-1 bg-white small">
		  				Работает: <b class="text-success">круглосуточно</b>
		  		</div>
			</div>
		</div>
	</div>



<div class="card m-1" style="max-width: 540px;">
		<div class="row no-gutters">	
		  	<div class="col-12">
		  		<div class="card-body p-1 small border-bottom">
		  				<h6 class="p-0 m-0"><a href="#" class="stretched-link">Намазхана в магазине Халал</a></h6>
		  				<section class="">Магазин Халал на Толе би</section>
		  				<section class=""><strong>Адрес:</strong> Абая Сайна</section>
		  		</div>
		  		<div class="card-footer border-0 mx-auto mt-auto p-1 bg-white small">
		  				Сейчас: Открыто <i class="icon-clock"></i><b class="text-success">10:00-19:00</b>
		  		</div>
			</div>
		</div>
</div>
<div class="card m-1" style="max-width: 540px;">
		<div class="row no-gutters">	
		  <div class="col-4">
		  	<img class="card-img" src="img/ccard">
		  	</div>
		  	<div class="col-8">
		  		<div class="card-body p-0">
		  				<h5 class="p-0 m-0 pl-1">Намазхана</h5>
		  				<section class="pl-1">Адрес:111,Толе би</section>
		  				<i class="icon-clock"></i>Открыто
		  				<section class="border-top pl-1">Интернет магазин восточных товаров</section>

		  		</div>
			</div>
		</div>
	</div>


	</main>

<?php get_footer( "single" );




















