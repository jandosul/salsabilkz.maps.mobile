<?php get_header("perehod"); ?> 

<header>

<!-- 	<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="<?php echo get_post_type_archive_link('namazhana');  ?>"><i class="icon-left-open"></i></a>
    s
<div class="col-8 text-left mr-4">
   <?php the_title(); ?>
 </div> -->

</nav>
</header>

<?php

$album = SCF::get('img');
$album_not_empty = false;
$album_list = array();

foreach($album as $img){
  $album_not_empty = $album_not_empty || $img!=''; 
  if($img!='')
    $album_list[] = $img;
}

if($album_not_empty):
?>

<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="0">
<?php
if(count($album_list)>1):    
?>  
  <ol class="carousel-indicators">
  <?php foreach ($album_list as $key => $value) { ?>
    <li data-target="#carouselExampleIndicators" data-slide-to="0"<?php echo $key==0?' class="active"':''?>></li>
  <?php } ?>
  </ol>
<?php
endif;
?>
  <div class="carousel-inner">
<?php
  foreach($album_list as $i => $img):
    $img_src = wp_get_attachment_image_src($img)[0];
    // print_r($img_src);
?>
    <div class="carousel-item<?php echo $i==0?' active':''?>">
      <img class="d-block w-100" src="<?php echo $img_src?>" alt="Первый слайд">
    </div>
<?php
  endforeach;
?>
  </div>
</div>

<?php
endif;
?>
<main>
		<?php if ( have_posts() ) : ?>

    <section id="circle-icons" class="bg-white">
      <div class="d-flex flex-row flex-wrap p-0 align-content-start">
			<?php
			// Start the Loop.
			while ( have_posts() ) :
				the_post();
?>
        <div class="col-12" >
        
          <h5><?php the_title()?></h5>
  
        </div>
<?php
			
			endwhile;
?>
      </div>
    </section>

<?php
		endif;
		?>
</main>
<!-- exper -->
<?php if(have_posts()): ?>

<div class="card  mx-1 border  dod mt-1">
      <a href="http://localhost/test/new.html#">
    
      <div class="card-body p-1 mb-5">
     
    <?php while (have_posts()): the_post();   ?>
         <nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <a class="nav-item nav-link active pr-3" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Адрес</a>
    <a class="nav-item nav-link pr-1" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Инфо</a>
    <a class="nav-item nav-link pr-1" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Режим работы</a>
  </div>
</nav>
<div class="tab-content" id="nav-tabContent">
  <div class="tab-pane fade show active pl-2" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab"><?php  echo SCF::get('address'); ?></div>
  <div class="tab-pane fade pl-2" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab"><?php the_content(); ?></div>
  <div class="tab-pane fade pl-2" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab"><?php get_sidebar(); ?></div>
</div>
      </div>
      </div>
    </div>
   <?php endwhile; ?> 
<?php else:echo "not";?>

<?php endif;?>
<?php get_footer(); ?> 







