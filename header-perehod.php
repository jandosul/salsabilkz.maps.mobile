


<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700&display=swap" rel="stylesheet">
<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="css/new.css"> -->
  <header>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="<?php echo get_home_url()?>"><i class="icon-left-open"></i></a>
    <div class="col-8 mr-3">
      <?php switch (true) {
      case is_singular("asxana"):
       $post_type_obj = get_post_type_object( 'asxana' );
        echo $post_type_obj->labels->all_items;
        break;
      case is_singular("namazhana"):
        $post_type_obj = get_post_type_object( 'namazhana' );
        echo $post_type_obj->labels->all_items;
        break;
      case is_post_type_archive("asxana"):
           $post_type_obj = get_post_type_object( 'asxana' );
           echo $post_type_obj->labels->all_items;
           break;
      case is_post_type_archive("namazhana"):
           $post_type_obj = get_post_type_object( 'namazhana' );
            echo $post_type_obj->labels->all_items;
            break;
      case is_tax( "taxasxana"):
           $ra = get_taxonomy_labels( "taxasxana" );
           echo get_queried_object()->name;
           break;
      default:
        echo "not";
        break;
    } ?>



    </div>


<!--   <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      
     <li class="nav-item active">
        <a class="nav-link" href="<?php echo get_home_url()?>">Home <span class="sr-only">(current)</span></a>
      </li>
      
</ul>
</div> -->
</nav>
</header>

<?php  wp_head(); ?>
</head>

<body>