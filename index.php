<?php get_header(); ?>

<header>

<nav class="navbar  navbar-expand-lg navbar-light bg-light justify-content-between  mt-1">
  <a href="" class="icon-home"></a>
  
  <h5 class="text-center"><a href="#"> Salsabil.Карты</a> </h5>
  
  <button class="navbar-toggler ml-2" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <?php  wp_nav_menu() ?>
</ul>
</div>
</nav>



</header>


<main>

  <div class="card mt-1" style="text-align: center">
  <div class="card-body">
    <section id="circle-icons" class="">
      <div class="d-flex flex-row flex-wrap p-3 align-content-start">
         
        <div class=" col-4 mb-1" >
      <?php the_post_thumbnail("sv77"); ?>
          <small>
            <img src="<?php echo get_template_directory_uri()?>/assets/img/ccard.png" class="photocat" >
            <a href="<?php echo get_post_type_archive_link('namazhana')?>">
            Намазахана
            </a>
          </small>
        </div>


        

        <div class=" col-4 mb-1" >
          <?php the_post_thumbnail("sv75"); ?>
          <small>
           <img src="<?php echo get_template_directory_uri()?>/assets/img/ccard.png" class="photocat" >
            <a href="<?php echo get_post_type_archive_link('asxana')?>">
                  Асхана
            </a>
          </small>
        </div>
      </div>
    </section>
  </div>
 </div>

 

      <?php

// WP_Query arguments
$args = array (
  'post_type'              => array( 'namazhana' ),
  'post_status'            => array( 'publish' ),
  'posts_per_page'         => 6,
  'order'                  => 'ASC',
  'orderby'                => 'menu_order',
  'orderby'                => 'ID',
);

// The Query
$namazhana = new WP_Query( $args );


// The Loop
if (! $namazhana->have_posts() ) {
?>
  <div class="card mt-1" style="text-align: center">
  <div class="card-body">
    <section id="circle-icons" class="">
      <div class="d-flex flex-row flex-wrap p-3 align-content-start">
        
<?php
  while ( $namazhana->have_posts() ) {
    $namazhana->the_post();
    // do something

?>
        <div class=" col-4 mb-1" >
          <?php the_post_thumbnail("sv75");?>
         
          <small><?php the_title(); ?></small>

        </div>
<?php    
  }
?>
      </div>
    </section>
  </div>
 </div>
<?php
} else {
  // echo "no postund";
}

// Restore original Post Data
wp_reset_postdata();

      ?>
    
<!-- 

	<div class="card mt-1" style="text-align: center">
  <div class="card-body">
		<section id="circle-icons" class="">
			<div class="d-flex flex-row flex-wrap p-3 align-content-start">
				<div class="mb-3 col-4">
					
					<a href="/wp2/namazhana/" class="stretched-link"><small>Намазхана</small></a>
				</div>
				<div class=" col-4" >
					
					<small>Намазхана</small>
				</div>
				<div class=" col-4">
					
					<small>Намазхана</small>
				</div>



      


				<div class=" col-4">
				
					<small>  ?></small>
				</div>
				<div class=" col-4">
          
					<small><; ?></small>
				</div>
				<div class=" col-4">
			
					<small>Намазхана</small>						
				</div>
			</div>
		</section>
	</div>
 </div>
 -->
<!--  <rect width="100%" height="100%" fill="#868e96"></rect>
 <div class="card" style="width: 18rem;">
  <svg class="bd-placeholder-img card-img-top" width="100%" height="180" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Image cap"><title>Placeholder</title><rect width="100%" height="100%" fill="#868e96"></rect><text x="50%" y="50%" fill="#dee2e6" dy=".3em">Image cap</text></svg>
  <div class="card-body">
    <h5 class="card-title">Card title</h5>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
  </div>
  <ul class="list-group list-group-flush">
    <li class="list-group-item">Cras justo odio</li>
    <li class="list-group-item">Dapibus ac facilisis in</li>
    <li class="list-group-item">Vestibulum at eros</li>
  </ul>
  <div class="card-body">
    <a href="#" class="card-link">Card link</a>
    <a href="#" class="card-link">Another link</a>
  </div>
</div>


 -->

<section id="brands-section" class="mb-1 mt-1" >
<div class="container bg-white shadow card pb-2 pt-2">
  <h2 class="pl-2 h4 text-center">Добавленные места</></h2>
    <div class="d-flex flex-row flex-nowrap overflow-auto">
      

<?php
$ggg = array (
  'post_type'              => array( 'namazhana' ),
  'post_status'            => array( 'publish' ),
  'posts_per_page'         => 6,
  'order'                  => 'DESC',
  'orderby'                => 'ID',
);?>
<?php $catquery = new WP_Query($ggg); ?>
 
<?php while($catquery->have_posts()) : $catquery->the_post(); ?>
<div class="col-5 m-0 px-1">
        <div class="card">
          <?php the_post_thumbnail("thumbnail", array("class" => "card-img-top")) ?>
          <div class="card-body pt-2">
            <h6 class="card-title brand-title p-0 m-0" title="Halagel"><a href="<?php the_permalink(); ?>" class="stretched-link"></a><?php the_title();?></h6>
          </div>
        </div>
      </div>      
               
<?php endwhile;
    wp_reset_postdata();
?>
        
      
      <!-- <div class="col-5 m-0 px-1">
        <div class="card">
          <img class="card-img-top img-fluid" src="//placehold.it/75x75" alt="Skin Doctor">
          <div class="card-body">
            <h5 class="card-title brand-title" title=""><a href="#" class="stretched-link"><?php the_title(); ("description"); ?></a></h5>
          </div>
        </div>
      </div>      
      
      <div class="col-5 m-0 px-1">
        <div class="card">
          <img class="card-img-top img-fluid" src="//placehold.it/75x75" alt="">
          <div class="card-body">
            <h5 class="card-title brand-title" title=""><a href="#" class="stretched-link">Esans</a></h5>
          </div>
        </div>
      </div>      
      
      <div class="col-5 m-0 px-1">
        <div class="card">
          <img class="card-img-top img-fluid" src="//placehold.it/75x75" alt="Parley">
          <div class="card-body">
            <h5 class="card-title brand-title" title="Parley"><a href="#" class="stretched-link">Parley</a></h5>
          </div>
        </div>
      </div>      
      
      <div class="col-5 m-0 px-1">
        <div class="card">
          <img class="card-img-top img-fluid" src="//placehold.it/75x75ds/2019/09/el-hawag-150x150.png" alt="">
          <div class="card-body">
            <h5 class="card-title brand-title" title=""><a href="#" class="stretched-link">El </a></h5>
          </div>
        </div>
      </div>      
      
      <div class="col-5 m-0 px-1">
        <div class="card">
          <img class="card-img-top img-fluid" src="//placehold.it/75x75" alt="">
          <div class="card-body">
            <h5 class="card-title brand-title" title=""><a href="#" class="stretched-link">Naseem </a></h5>
          </div>
        </div>
      </div>      

  -->
    </div>
</div>
</section>

<!--  <div class="col-5 m-0 px-1">
        <div class="card">
          <img class="card-img-top img-fluid" src="//placehold.it/75x75" alt="Parley">
          <div class="card-body">
            <h5 class="card-title brand-title" title="Parley"><a href="#" class="stretched-link">Parley</a></h5>
          </div>
        </div>
      </div>      
      
      <div class="col-5 m-0 px-1">
        <div class="card">
          <img class="card-img-top img-fluid" src="//placehold.it/75x75ds/2019/09/el-hawag-150x150.png" alt="">
          <div class="card-body">
            <h5 class="card-title brand-title" title=""><a href="#" class="stretched-link">El </a></h5>
          </div>
        </div>
      </div>      
      
      <div class="col-5 m-0 px-1">
        <div class="card">
          <img class="card-img-top img-fluid" src="//placehold.it/75x75" alt="">
          <div class="card-body">
            <h5 class="card-title brand-title" title=""><a href="#" class="stretched-link">Naseem </a></h5>
          </div>
        </div>
      </div>      

  --> 


</main>





<?php get_footer(); ?> 
