<?php get_header("perehod"); ?> 

<!-- <header>
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="<?php echo get_home_url()?>"><i class="icon-left-open"></i></a>
    <?php
$post_type_obj = get_post_type_object( 'asxana' );
// print_r($post_type_obj->labels);
echo $post_type_obj->labels->all_items; 
?>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo get_home_url()?>">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      
</ul>
</div>
</nav>
</header>
 -->

<?php 
$terms = get_terms( [
  'taxonomy' => 'taxasxana',
  'hide_empty' => false,
] );  
// print_r($terms);
?>
   <div class="card mt-1 ">
  <div class="card-body d-flex justify-content-around ">
 <?php foreach($terms as $term): ?>
    <a href="<?php echo get_term_link( $term )?>" class="btn btn-primary "><?php echo $term->name; ?></a>
 <?php endforeach; ?>
  </div>
 </div>

<main>
  <?php  get_post_type_object( "asxana" );  ?>
	
</main>



  <?php if (have_posts()): while (have_posts()):  the_post();?>
<div class="card m-1" style="max-width: 540px;">
    <div class="row no-gutters">  
      <div class="col-4">
        <?php the_post_thumbnail( "card-img", array("class=> img") );  ?>
        </div>
        <div class="col-8 d-flex align-items-start flex-column ">
          <div class="card-body p-1 small">
              <h6 class="m-0"><a href="<?php the_permalink( ) ?>" class="stretched-link"><?php the_title();  ?></a></h6>
              <section class="border-bottom"></section>
              <section class=""><strong>Адрес:</strong> <?php  echo SCF::get('address');  ?></section>
          </div>
          <div class="card-footer border-0 mt-auto p-1 bg-white small">
              Сейчас: <span class="text-danger font-weight-bold">закрыто</span>
          </div>
      </div>
    </div>
  </div>
   <?php endwhile;
  endif; ?>



<?php get_footer(); ?> 




